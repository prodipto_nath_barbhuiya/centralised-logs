Send ALB logs from S3 bucket to ElasticSearch using AWS Lambda

This directory contains terraform module to send ALB logs from S3 bucket to ElasticSearch using AWS Lambda

Particularly it creates:

Lambda function that does the sending
IAM role and policy that allows access to ES
S3 bucket notification that triggers the lambda function when an S3 object is created in the bucket.
(Only when your Lambda is deployed inside a VPC) Security group for Lambda function