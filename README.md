# centralised-logs

centralised-logs

AWS Centralised Logs with Terraform

AWS ElasticSearch Service, Lambda and CloudWatch logs can be used to provide a simple and cost-effective centralised log management service.

+------------+        +----------+          +----------------+
| Server     |        |Cloudwatch|  lambda  |  ElasticSearch | clean 
| Instance   |    +--->   Logs   +---------->    Service     <-----
++----------++    |   |          |          |                |lambda   
||Cloudwatch||    |   +----------+          |                |
||  Agent   |-----+                         |                |
|+----------+|        +----------+  lambda  |    +--------+  |
+------------+        |ELB Logs  +---------->    | Kibana |  |
                      |  (S3)    |          |    |        |  |
                      |          |          |    +----^---+  |
                      +----------+          +---------|------+
                                                      |
                                                      |
                                     +----------------|------+
                                     |Client   +---------+   |
                                     |Work-    |Proxy (optional)
                                     |station  +----^----+   |
                                     |              |        |
                                     |         +----+----+   |
                                     |         |         |   |
                                     |         | Browser |   |
                                     |         |         |   |
                                     |         +---------+   |
                                     +-----------------------+


AWS ElasticSearch Service, Lambda and CloudWatch logs can be used to provide a simple and cost-effective centralised log management service.
1)	Server instances need to be setup to send logs to AWS CloudWatch logs via the the AWS CloudWatch logs agent

2)	Currently manually in the AWS console each CloudWatch log needs to be configured to call the AWS supplied lambda to load the data into the AWS ElasticSearch cluster.


3)	 The AWS ElasticSearch service should be setup in a private VPC so it cannot be accessed via the public internet.

4)	Finally the lambdas have limitations in the amount of data they can process. To overcome limitations use Kinesis Firehose
